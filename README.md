Zavodnick, Zavodnick & Lasky, LLC is one of the top personal injury law firms in Philadelphia, PA. Our attorneys have over 80 years of combined experience helping injury victims in Philadelphia and its vicinities. When it comes to personal injury representation, few firms have as much experience as Zavodnick, Zavodnick & Lasky, LLC. 

Some of our most notable results include a $4.8 million settlement in a catastrophic motor vehicle accident, a $1.1 million settlement in a drunk driving accident, a $780,000 settlement in a wrongful death claim, and a $500,000 settlement in a slip and fall injury. Though these are some of our most notable results, the attorneys at Zavodnick, Zavodnick & Lasky, LLC handle all kinds of personal injury cases, no matter how big or small. 

Ryan Zavodnick is a hands-on attorney who is committed to going the extra mile for his clients. He is licensed to practice law in Pennsylvania, New Jersey, and Delaware. His area of expertise is slip and fall accidents, car accidents, motorcycle accidents, and workplace injuries. 

Todd Lasky received his J.D. from the prestigious University of Pennsylvania School of Law and has been fighting for injury victims ever since. Since 2004, Mr. Lasky has been recognized as a Pennsylvania Rising Star by Super Laywers a title that is given to less than 2.5 percent of the lawyers of Pennsylvania. His areas of expertise are car accidents, injuries in the workplace, and slip and fall accidents. 

At Zavodnick, Zavodnick & Lasky, LLC, there is no fee unless we win. This means that if you choose to hire us, we will not charge a fee unless we are able to secure a financial compensation on your behalf. This means that you can focus on your recovery while we fight to secure your compensation. 

We offer a free, no-obligation first consultation. Call us today to discuss your case and see how a personal injury lawyer from Zavodnick, Zavodnick & Lasky, LLC can help you.

Zavodnick, Zavodnick & Lasky, LLC

123 S Broad St #1220
Philadelphia, PA 19109

(215) 875-7030

https://www.zavodnicklaw.com